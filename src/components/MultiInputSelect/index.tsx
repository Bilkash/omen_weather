import React, { useEffect, useState } from "react";
import Image from "next/image";

import { Filters, OptionsType, OptionType } from "@/types";
import ArrowSvg from "@/assets/arrow.svg";

import styles from "./index.module.css";

interface DropdownProps {
  options?: OptionsType;
  setFilter: React.Dispatch<React.SetStateAction<Filters | null>>;
  placeholder: string;
  filter: "countries" | "tempMin" | "tempMax";
  numeric?: boolean
}

const defaultValue = undefined;

export default function MultiInputSelect ({ options, setFilter, placeholder, filter, numeric }: DropdownProps) {
	const [selectedOptions, setSelectedOptions] = useState<OptionsType>([]);
	const [filterText, setFilterText] = useState("");
	const [open, setOpen] = useState(false);

	const handleOptionClick = (option: OptionType) => {
		if (filter === "countries") {
			const isSelected = selectedOptions.some((selectedOption) =>
				isOptionEqual(selectedOption, option)
			);

			if (isSelected) {
				const updatedOptions = selectedOptions.filter((selectedOption) =>
					!isOptionEqual(selectedOption, option)
				);
				setSelectedOptions(updatedOptions);
			} else {
				setSelectedOptions([...selectedOptions, option]);
			}
		} else {
			setSelectedOptions([option]);
		}

		if (selectedOptions.length === 1 && isOptionEqual(selectedOptions[0], option)) {
			setSelectedOptions([]);
		}

		setFilterText("");
	};

	const isOptionEqual = (optionA: OptionType, optionB: OptionType) => {
		return Object.entries(optionA).every(([key, value]) => {
			if (key === "id" && numeric) {
				return optionB[key] === value;
			}
			const filterRegex = new RegExp(`^${filterText}`, "i");
			return filterRegex.test(value.toString()) && optionB[key] === value;
		});
	};

	const filteredOptions = options?.filter((option) =>
		Object.values(option).some((value) => {
			const filterRegex = new RegExp(`^${filterText}`, "i");
			return filterRegex.test(value.toString());
		})
	) || [];

	const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		if (numeric) {
			if (!event.target.value) {
				setSelectedOptions([]);
			} else {
				setSelectedOptions([{ name: event.target.value, id: +event.target.value }]);
			}
		}
		setFilterText(event.target.value);
	};

	useEffect(() => {
		if (selectedOptions.length > 0) {
			setFilter((prev) => {
				if (!prev) {
					return {
						[filter]: numeric ? selectedOptions[0].id : selectedOptions.map((it) => it.name),
						...(defaultValue && { [`${filter}`]: defaultValue }),
					};
				} else {
					return {
						...prev,
						[filter]: filter === "countries" ? selectedOptions.map((it) => it.name) : selectedOptions[0].id,
					};
				}
			});
		} else {
			setFilter((prev) => ({ ...prev, [filter]: defaultValue }));
		}
	}, [selectedOptions]);

	const renderDropdown = () => {
		if (numeric) {
			return null;
		} else {
			if (open) {
				return (
					<div className={styles.dropDown}>
						{filteredOptions.map((option, index) => (
							<div
								key={index}
								onClick={() => handleOptionClick(option)}
								className={`${styles.dropDownItem} ${selectedOptions.some((selectedOption) =>
									isOptionEqual(selectedOption, option)
								)
									? `${styles.dropDownItemSelected}`
									: ""}`}
							>
								{option.name}
							</div>
						))}
					</div>
				);
			} else {
				return null;
			}
		}
	};

	return (
		<div className={styles.selectWrapper}>
			{numeric ? null : (
				<div className={styles.selected}>
					{selectedOptions.map((option, index) => (
						<div
							className={styles.selectedItems}
							onClick={() => handleOptionClick(option)}
							key={index}
						>
							{option.name}
						</div>
					))}
				</div>
			)}

			<input
				className={`${styles.input} ${numeric ? styles.numericInput : ""}`}
				type={filter === "countries" ? "text" : "number"}
				value={filterText}
				onChange={handleInputChange}
				placeholder={placeholder}
				onClick={() => setOpen(true)}
			/>

			{numeric ? null : (
				<div
					className={styles.arrowWrapper}
					onClick={() => setOpen(!open)}
				>
					<Image
						className={`${styles.arrowClose} ${open ? styles.arrowOpen : ""}`}
						src={ArrowSvg}
						alt={"arrow"}
					/>
				</div>
			)}

			{renderDropdown()}
		</div>
	);
};
