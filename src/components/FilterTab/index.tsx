import React, { useState } from "react";

import { countries } from "@/constansts/countries";
import { Filters, OptionsType, OptionType } from "@/types";
import MultiInputSelect from "@/components/MultiInputSelect";

import styles from "./index.module.css";

type FilterType = {
  filter: Filters | null;
  setFilter: React.Dispatch<React.SetStateAction<Filters | null>>;
}

export default function FilterTab({ filter, setFilter }: FilterType) {
	const countriesKeys = Object.keys(countries);

	const countriesNames = countriesKeys.map((it, index) => ({
		name: it,
		id: index,
	}));

	return (
		<div className={styles.filterWrapper}>
			<MultiInputSelect
				options={countriesNames}
				setFilter={setFilter}
				filter={"countries"}
				placeholder={"Country"}
			/>

			<MultiInputSelect
				setFilter={setFilter}
				filter={"tempMin"}
				placeholder={"Min"}
				numeric
			/>

			<MultiInputSelect
				setFilter={setFilter}
				filter={"tempMax"}
				placeholder={"Max"}
				numeric
			/>
		</div>
	);
}
