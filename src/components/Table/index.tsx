import React from "react";

import { CitiesData, CityData, DailyWeather, Filters } from "@/types";

import styles from "./index.module.css";

type Table = {
  data: CitiesData | null;
  setDataForChart: React.Dispatch<React.SetStateAction<DailyWeather | null>>
}

export default function Table({ data, setDataForChart }: Table) {

	if (data !== null) {
		return (
			<div className={styles.table}>
				<div className={styles.tableHeader}>
					<div className={styles.cellHeader}>City</div>
					<div className={styles.cellHeader}>Temparature max</div>
					<div className={styles.cellHeader}>Temparature min</div>
					<div className={styles.cellHeader}>Wind direction</div>
				</div>
				{data.map((city) => {
					return (
						<div
							className={styles.row}
							key={city[0]}
							onClick={() => setDataForChart(city[1])}
						>
							<div className={styles.cell}>{city[0]}</div>
							<div className={`${styles.cell} ${styles.cellNumber}`}>
								{city[1].daily.temperature_2m_max[city[1].daily.temperature_2m_max.length - 1]}
							</div>
							<div className={`${styles.cell} ${styles.cellNumber}`}>
								{city[1].daily.temperature_2m_min[city[1].daily.temperature_2m_min.length - 1]}
							</div>
							<div className={`${styles.cell} ${styles.cellNumber}`}>
								{city[1]
									.daily
									.winddirection_10m_dominant[city[1].daily.winddirection_10m_dominant.length - 1]}
							</div>
						</div>
					);
				})}
			</div>
		);
	} else {
		return null;
	}
}
