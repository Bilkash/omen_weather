import React, { useEffect, useRef, useState } from "react";
import * as d3 from "d3";
import moment from "moment";

import { DailyWeather } from "@/types";

import styles from "./index.module.css";

type Chart = {
  cityData: DailyWeather | null
}

export default function Chart({ cityData }: Chart) {
	const svgRef = useRef(null);
	const [data, setData] = useState<{date: string, temp: number}[] | null>(null);

	const width = 550,
		height = 400,
		marginLeft = 25,
		marginBottom = 25;
	
	useEffect(() => {
		if (cityData) {
			const data = cityData.daily.time.map((it, index) => {
				return {
					date: moment(it).format("ddd"),
					temp: Math.floor(
						(cityData.daily.temperature_2m_min[index] +  cityData.daily.temperature_2m_max[index]) / 2
					)
				};
			});
			
			const rx = 9;
			const ry = 9;
			
			setData(data);
			const svgElement = d3.select(svgRef.current);
			svgElement.selectAll("*").remove();
			const x = d3.scaleBand()
				.domain(data.map(d => d.date))
				.range([0, width]);
			const y = d3.scaleLinear()
				.domain([0, 45])
				.range([height, 0]);
			const yAxis = d3.axisLeft(y).tickSize(width);
			svgElement.attr("width", width);
			svgElement.attr("height", height);
			svgElement.append("g")
				.attr("transform", `translate(25,${height - 20})`)
				.attr("class", "xAxis")
				.call(d3.axisBottom(x))
				.call(g => g.select(".domain").remove());
			svgElement.append("g")
				.attr("transform", "translate(575, -20)")
				.attr("class", "yAxis")
				.call(d3.axisLeft(y).tickSize(width));
			const bars = svgElement.selectAll(".bar")
				.data(data)
				.enter().append("path")
				.style("fill", "#b3fc4f")
				.attr("d", d => `
                    M${x(d.date)},${y(d.temp) + ry}
                    a${rx},${ry} 0 0 1 ${rx},${-ry}
                    h${11}
                    a${rx},${ry} 0 0 1 ${rx},${ry}
                    v${height - y(d.temp) - ry}
                    h${-30}Z
                `)
				.attr("transform", "translate(50, -20)");
			svgElement.append("text")
				.attr("class", "xLabel")
				.attr("text-anchor", "end")
				.attr("x", width)
				.attr("y", 15)
				.style("fill", "#fff")
				.text("Temperature °C");
			svgElement.append("rect")
				.attr("class", "labelColor")
				.attr("x", width -150)
				.attr("y", 0)
				.attr("width", 20)
				.attr("height", 20)
				.attr("rx", 5)
				.style("fill", "#b3fc4f");
			const tiks = svgElement.selectAll(".tick")
				.style("font-size", "14px");
			const tickLine = svgElement.selectAll(".tick line")
				.style("stroke-dasharray", "15 10")
				.style("stroke-width", 3)
				.style("stroke", "#313131");
		}
	}, [cityData]);

	return (
		<div className={styles.chartWrapper}>
			<svg ref={svgRef} />
		</div>
	);
}
