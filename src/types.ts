export type Cities = {
  [city: string]: CityCoordinates;
};

export type Countries = {
  [country: string]: string[]
}

type CityCoordinates = [string, string];

export type CitiesData = CityData[]

export type CityData = [string, DailyWeather];

export type DailyWeather = {
  daily: {
    temperature_2m_max: number[];
    temperature_2m_min: number[];
    time: string[];
    winddirection_10m_dominant: number[];
  }
  daily_units: {
    temperature_2m_max: string;
    temperature_2m_min: string;
    time: string;
    winddirection_10m_dominant: string;
  };
  elevation: number;
  generationtime_ms: number;
  latitude: number;
  longitude: number;
  timezone: string;
  timezone_abbreviation: string;
  utc_offset_seconds: number;
};

export type Filters = {
  countries?: string[];
  tempMin?: number;
  tempMax?: number;
}

export type OptionType = {
  name: string;
  id: number;
};
export type OptionsType = Array<OptionType>;
