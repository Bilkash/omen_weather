import {Countries} from "@/types";

export const countries: Countries = {
  Ukraine: ["Kyiv", "Dnipro", "Odessa"],
  England: ["London"],
  Finland: ["Helsinki"],
  Italy: ["Venice"],
  Nederland: ["Amsterdam"],
  Luxemburg: ["Luxembourg"]
}
