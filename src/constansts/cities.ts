import {Cities} from "@/types";

export const cities: Cities = {
  Kyiv: ["50.4547", "30.5238"],
  Dnipro: ["48.4666", "35.0407"],
  Odessa: ["46.4857", "30.7438"],
  London: ["51.5085", "-0.1257"],
  Helsinki: ["60.1695", "24.9354"],
  Venice: ["45.4371", "12.3326"],
  Luxembourg: ["49.75", "6.1667"],
  Amsterdam: ["52.374", "4.8897"],
}
