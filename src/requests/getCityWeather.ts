import axios, { AxiosResponse } from "axios";

import { DailyWeather } from "@/types";

export default function getCityWeather(lat: string, lon: string) {
	return axios({
		url: `${process.env.NEXT_PUBLIC_API_URL}?latitude=${lat}&longitude=${lon}&daily=temperature_2m_max,temperature_2m_min,winddirection_10m_dominant&timezone=auto&past_days=6&forecast_days=1`,
		method: "get",
		responseType: "json"
	})
		.then((response: AxiosResponse<DailyWeather>) => {
			return response.data;
		})
		.then((result) => result)
		.catch(e => console.log(e.message));
}
